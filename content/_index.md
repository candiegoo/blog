+++

+++
## Cleverest

Cleverest finds creative solutions for technical problems. We offer easy to understand advice, solutions, training, and on-site assistance for people and small businesses at a decent price. You only pay us if you need us to do something for you. If you feel that your issue was not resolved to your satisfaction you get a refund. 

We are a small startup and we plan to grow with your help by word of mouth. 

You have questions, we have answers.