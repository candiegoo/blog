---
title: Cleverest
subtitle: You've got questions, we've got answers?
comments: false

---
After 18 years working with technology I came to the realization that most people just need assistance using the tools that they have to accomplish something. They don't just need their computer repaired or websites built anymore. 

Cleverest finds creative solutions for technical problems. We offer easy to understand advice, solutions, training, and on-site assistance for people and small businesses at a decent price. You only pay us if you need us to do something for you. If you feel that your issue was not resolved to your satisfaction you get a refund. 

We are a small startup and we plan to grow with your help by word of mouth.